package com.itau.exemplospring.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	
	@RequestMapping("")
	public String hello() {
		return "hello";
	}
	
	@RequestMapping(method=RequestMethod.POST, path="")
	public String receber() {
		return "Recebi um POST";
	}
	
	@RequestMapping("/teste")
	public String teste() {
		return "teste";
	}
}
